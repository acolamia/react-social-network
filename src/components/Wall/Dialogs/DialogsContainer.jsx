import React from "react";
import {sendMessageCreator, updateNewMessageBodyCreator} from "../../../redux/dialogsReducer";
import Dialogs from "./Dialogs";

const DialogsContainer = (props) => {
    let dialogPage = props.store.getState().dialogPage;

    let onSendMessageClick = () => {
        props.store.dispatch(sendMessageCreator())
    };

    let onNewMessageChange = (text) => {
        let action=updateNewMessageBodyCreator(text);
        props.store.dispatch(action);
    };

    return (
       <Dialogs updateNewMessage={onNewMessageChange}
                addMessage={onSendMessageClick}
                dialogPage={dialogPage}/>
    )
};

export default DialogsContainer;