import React from "react";
import selfStyle from './Header.module.css';
import logo from './../../images/logo.jpg';

const Header=()=>{
    return <header className={selfStyle.header}>
        <img src={logo}/>
    </header>
}

export default Header;