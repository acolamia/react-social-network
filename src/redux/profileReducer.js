const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';

let initialState = {
    posts: [
        {id: 1, message: 'Hi this is post', likesCount: 15},
        {id: 2, message: 'Its post', likesCount: 9},
        {id: 3, message: 'jfgnkdfngldlkfgml', likesCount: 1},
        {id: 4, message: '7547 45y h54h 54h', likesCount: 33}
    ],
    newPostText: ''
};

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            let newPost = {
                id: 5,
                message: state.newPostText,
                likesCount: 0
            };
            state.posts.push(newPost);
            state.newPostText = '';
            return state;
        case UPDATE_NEW_POST_TEXT:
            state.newPostText = action.newText;
            return state;
        default:
            return state;
    }
};

export const addPostCreator = () => {
    return (
        {
            type: ADD_POST
        }
    )
};

export const updatePostCreator = (text) => {
    return (
        {
            type: UPDATE_NEW_POST_TEXT,
            newText: text
        }
    )
};

export default profileReducer;