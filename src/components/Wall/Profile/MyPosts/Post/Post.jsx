import React from "react";
import selfStyle from './Post.module.css';

const Post = (props) => {
    return (
        <div className={selfStyle.item}>
            <img
                src={'https://interactive-examples.mdn.mozilla.net/media/examples/grapefruit-slice-332-332.jpg'}></img>
            {props.message}
            <div>
                <span>{props.likeCount} </span>
                <span>like</span>
            </div>
        </div>
    )
}

export default Post;