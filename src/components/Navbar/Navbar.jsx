import React from "react";
import selfStyle from './Navbar.module.css';
import {NavLink} from "react-router-dom";

const Navbar = (props) => {

    return <nav className={selfStyle.nav}>
        <div>
            <div className={selfStyle.item}>
                <NavLink to={"/profile"} activeClassName={selfStyle.activeLink}>Profile</NavLink>
            </div>
            <div className={selfStyle.item}>
                <NavLink to={"/dialogs"} activeClassName={selfStyle.activeLink}>Messages</NavLink>
            </div>
            <div className={selfStyle.item}>
                <NavLink to={"/news"} activeClassName={selfStyle.activeLink}>News</NavLink>
            </div>
            <div className={selfStyle.item}>
                <NavLink to={"/music"} activeClassName={selfStyle.activeLink}>Music</NavLink>
            </div>
            <div className={selfStyle.item}>
                <NavLink to={"/settings"} activeClassName={selfStyle.activeLink}>Settings</NavLink>
            </div>
        </div>
    </nav>
}

export default Navbar;