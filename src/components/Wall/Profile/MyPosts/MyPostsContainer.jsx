import React from "react";
import {updatePostCreator, addPostCreator} from "../../../../redux/profileReducer";
import MyPosts from "./MyPosts";

const MyPostsContainer = (props) => {
    let profilePage = props.store.getState().profilePage;

    let addPost = () => {
        props.store.dispatch(addPostCreator());
    };

    let onPostChange = (text) => {
        let action = updatePostCreator(text);
        props.store.dispatch(action);
    };

    return (
        <MyPosts updateNewPostText={onPostChange}
                 addPost={addPost}
                 posts={profilePage.posts}
                 newPostText={profilePage.newPostText}
        />
    )
};

export default MyPostsContainer;