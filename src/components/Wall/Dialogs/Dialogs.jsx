import React from "react";
import styles from "./Dialogs.module.css";
import Wall from "../Wall.module.css";
import DialogItem from "./DialogItem/DialogItem";
import Message from "./Message/Message";

const Dialogs = (props) => {
    let data = props.dialogPage;

    let dialogElements = data.dialogs.map(name => <DialogItem name={name.name} id={name.id}/>);
    let messageElements = data.messages.map(message => <Message message={message.message}/>);
    let newMessageBody = data.newMessageBody;

    let onSendMessageClick = () => {
        props.addMessage();
    };

    let onNewMessageChange = (event) => {
        let body = event.target.value;
        props.updateNewMessage(body);
    };

    return (
        <div className={Wall.wall}>
            <div className={styles.dialogs}>
                <div className={styles.dialogItems}>
                    {dialogElements}
                </div>

                <div className={styles.messages}>
                    <div>
                        {messageElements}
                    </div>
                    <div>
                        <div><textarea value={newMessageBody} onChange={onNewMessageChange}
                                       placeholder='Enter your message' cols="100" rows="5"></textarea></div>
                        <div>
                            <button onClick={onSendMessageClick}>Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default Dialogs;