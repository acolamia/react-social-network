import React from 'react';
import './App.css';
import Header from "./components/Header/Header";
import Navbar from "./components/Navbar/Navbar";
import Profile from "./components/Wall/Profile/Profile";
import {Route} from "react-router-dom";
import Music from "./components/Wall/Music/Music";
import Settings from "./components/Wall/Settings/Settings";
import News from "./components/Wall/News/News";
import NavbarRight from "./components/NavbarRight/NavbarRight";
import DialogsContainer from "./components/Wall/Dialogs/DialogsContainer";

const App = (props) => {
    // debugger;
    return (
        <div className={'app-wrapper'}>
            <Header/>
            <Navbar state={props.state.navbarPage}/>
            <NavbarRight/>
            <div className={'app-wrapper-content'}>
                <Route path={'/profile'} render={() => <Profile store={props.store}/>}/>
                <Route path={'/dialogs'} render={() => <DialogsContainer store={props.store}/>}/>
                <Route path={'/news'} render={() => <News news={props.state}/>}/>
                <Route path={'/music'} render={() => <Music music={props.state}/>}/>
                <Route path={'/settings'} render={() => <Settings settings={props.state}/>}/>
            </div>
        </div>
    );
}

export default App;
