import React from "react";
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import ProfileCss from './Profile.module.css'
import WallCss from './../Wall.module.css'
import MyPostsContainer from "./MyPosts/MyPostsContainer";

const Profile = (props) => {
    return (
        <div className={WallCss.wall}>
            <div className={ProfileCss.profile}>
                <ProfileInfo/>
                <MyPostsContainer store={props.store}/>
            </div>
        </div>
    )
}

export default Profile;