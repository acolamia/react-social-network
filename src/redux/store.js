import profileReducer from "./profileReducer";
import dialogsReducer from "./dialogsReducer";

let store = {
    state: {
        profilePage: {
            posts: [
                {id: 1, message: 'Hi this is post', likesCount: 15},
                {id: 2, message: 'Its post', likesCount: 9},
                {id: 3, message: 'jfgnkdfngldlkfgml', likesCount: 1},
                {id: 4, message: '7547 45y h54h 54h', likesCount: 33}
            ],
            newPostText: ''
        },

        dialogPage: {
            dialogs: [
                {id: 1, name: 'Dima'},
                {id: 2, name: 'Sveta'},
                {id: 3, name: 'Kolya'},
                {id: 4, name: 'Vasya'},
                {id: 5, name: 'Oleg'}
            ],
            messages: [
                {id: 1, message: 'Hi'},
                {id: 2, message: 'Bye'},
                {id: 3, message: 'Why?'}
            ], newMessageBody: ""
        },

        navbarPage: {
            friends: [
                {id: 1, name: 'Dima'},
                {id: 3, name: 'Sveta'},
                {id: 2, name: 'Kolya'},
                {id: 4, name: 'Vasya'},
                {id: 5, name: 'Oleg'}
            ]
        }
    },
    getState() {
        return this.state;
    },
    subscribe(observer) {
        this.rerenderEntireTree = observer;
    },
    dispatch(action) {
        this.state.profilePage = profileReducer(this.state.profilePage, action);
        this.state.dialogPage = dialogsReducer(this.state.dialogPage, action);

        this.rerenderEntireTree(this.state);
    },
    rerenderEntireTree() {
    }
};

export default store;