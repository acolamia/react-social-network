import React from "react";
import ProfileInfoCss from './ProfileInfo.module.css'

const ProfileInfo = () => {
    return (
        <div className={ProfileInfoCss.profileInfo}>
            <div><img
                src={'https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Definitions_of_TV_standards.jpg/1280px-Definitions_of_TV_standards.jpg'}/>
            </div>
            <div className={ProfileInfoCss.descriptionBlock}>ava+description</div>
        </div>
    )
}

export default ProfileInfo;